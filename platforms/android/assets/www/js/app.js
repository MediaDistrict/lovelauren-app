//Global Vars
var homeStart = 0;
var atwStart = 0;
var _blogPostList = '';
var _atwPostList = '';

var blogPosts = [];
var atwPosts = [];

var _title = '';

var _aboutText = '';
var _aboutImage = '';
var _aboutTitle = '';

var Utils = {
  query: function(url) {
    return $.get(url, function(data) {}, 'json');
  },
  email: function() {
    window.plugins.socialsharing.shareViaEmail(
      'I love your blog!', //message
      'Email via app', //subject
      ['lauren@lovelauren.eu'], // TO
      null, // CC
      null, // BCC
      null, // FILES
      Utils.emailSuccess,
      Utils.emailError
    );
  },
  emailSuccess: function() {
    console.log('success');
  },
  emailError: function() {
    console.log('error');
  },
  loadURL: function(url) {

    myApp.confirm('You are about to visit an external website', 'Confirm',
      function () {
        var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no');
      },
      function () {
        return false;
      }
    );

  }
}

var Builder = {
    about: function() {
      // href="about.html"
      mainView.router.loadContent('<div class="navbar">' +
                                    '<div class="navbar-inner">' +
                                      '<div class="left"><a href="#" class="link icon-only open-panel"> <i class="fa fa-bars"></i></a></div>' +
                                      '<div class="center sliding">About Me</div>' +
                                    '</div>' +
                                  '</div>' +
                                  '<div class="pages">' +
                                    '<div data-page="about" class="page">' +
                                      '<div class="page-content">' +
                                        '<header style="background-image: url('+ _aboutImage +')"></header>' +
                                        '<section class="aboutContent">' +
                                          '<h2>'+ _aboutTitle +'</h2>' +
                                          _aboutText +
                                        '</section>' +
                                      '</div>' +
                                    '</div>' +
                                  '</div>');
    }
}

var populatePosts = {
  blog: function() {
    $.when(Utils.query('https://www.lovelauren.eu/_app/getList/?start=' + homeStart)).then(function(data) {
      _blogPostList = '';
      _title = 'Blog';
      _aboutImage = data.about.image;
      _aboutText = data.about.text;
      _aboutTitle = data.about.title;

      blogPosts.push(data.posts);

      $.each(data.posts, function(j, k) {
        _blogPostList += '<li data-id="'+ k.content.id +'" data-type="blog" class="blogItem">' +
        '<h2 class="title">'+ k.content.post_title +'</h2>' +
        '<p class="dateTime">'+ k.content.posted_date +'<span>'+ k.content.posted_time +'</span></p>' +
        '<img src="'+ k.images[0] +'" alt="" id="img" />' +
        '<section class="snippet">' +
        k.excerpt.replace('...', '') +
        '</section>' +
        '<span class="readMore">Read More</span>' +
        '</li>';
      });

        $('#blogList').append(_blogPostList);

        var blogInview = new Waypoint.Inview({
          element: $('.trigger'),
          context: $('#blogContent'),
          enter: function(direction) {

            if (direction == 'down') {
              homeStart++
              populatePosts.blog();
              console.log('calling blog');
            }

          }
        });
    });

  },
  atw: function() {
    _atwPostList = '';
    $.when(Utils.query('https://www.lovelauren.eu/_app/atw/?start=' + atwStart)).then(function(data) {
      _title = 'Around The World';
      atwPosts.push(data.posts);
        $.each(data.posts, function(i, e) {
          var _split = e.content.created_at.replace(' ', '');
          var _date = _split.substring(0, 10);
          var _time = _split.substring(10, _split.length);

          _atwPostList += '<li class="blogItem" data-type="atw" data-id="'+ e.content.id +'">' +
                      '<h2 class="title">'+ e.content.post_title +'</h2>' +
                      '<p class="dateTime">'+ _date +'<span>'+ _time.substring(0, 5) +'</span></p>' +
                      '<img src="'+ e.images[0] +'" alt="" />' +
                      '<section class="snippet">' +
                        e.excerpt.replace('...', '') +
                        '</section>' +
                        '<span class="readMore">Read More</span>' +
                      '</li>';

        });

        $('#atwList').append(_atwPostList);

        var ATWInview = new Waypoint.Inview({
          element: $('#atwTrigger'),
          context: $('#atwContent'),
          enter: function(direction) {
            console.log('here');
            if (direction == 'down') {
              atwStart++
              populatePosts.atw();
              console.log('calling atw');
            }

          }
        });
    });
  }
}


var myApp = new Framework7();
var $$ = Dom7;
var mainView = myApp.addView('.view-main', { dynamicNavbar: true });

var notificationOpenedCallback = function(jsonData) {
	console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
};

document.addEventListener('deviceready', function ()
{
	var notificationOpenedCallback = function(jsonData) {
		//alert('notificationOpenedCallback: '  JSON.stringify(jsonData));
	};

	//window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
	window.plugins.OneSignal.registerForPushNotifications();
	window.plugins.OneSignal.startInit("af09c0e8-e92f-4d94-ba21-92305af0a5e3", "1092843836850").handleNotificationOpened(notificationOpenedCallback).endInit();
}, false);


populatePosts.blog();

$('body').on('click', '.blogItem', function() {
  var _id = $(this).data('id');
  var _type = $(this).data('type');
  var _data = '';
  var _img = '';
  var _postList = '';

  var _split;
  var _date;
  var _time;

  if (_type == 'blog') {
    _postList = blogPosts;
  } else {
    _postList = atwPosts;
  }

  $.each(_postList, function(i, e) {
      $.each(e, function(j, k) {
        if (k.content.id == _id) {
          _data = k;

          $.each(_data.images, function(i, e) {
            _img += '<img src="'+ e +'" />'
          });

        }
      });
  });

  mainView.router.loadContent('<div class="navbar">' +
                                '<div class="navbar-inner">' +
                                  '<div class="left"><a href="#" class="back link"><i class="fa fa-long-arrow-left fa-2x"></i></a></div>' +
                                  '<div class="center sliding">'+ _title +'</div>' +
                                  '<div class="right">' +
                                    '<a href="#" class="link icon-only open-panel"> <i class="fa fa-bars"></i></a>' +
                                  '</div>' +
                                '</div>' +
                              '</div>' +
                              '<div data-page="newspost" class="page">' +
                                '<div class="page-content">' +
                                '<div class="blog">' +
                                  '<h2 class="title"><a href="#">'+ _data.content.post_title +'</a></h2>' +
                                  '<p class="dateTime">'+ _data.content.posted_date +'<span>'+ _data.content.posted_time +'</span></p>' +
                                  _img +
                                    '<section class="snippet">' +
                                    _data.content.post_content +
                                    '</section>' +
                                '</div>' +
                                '</div>' +
                              '</div>');

});

$('body').on('click', '#facebook', function() {
  var ref = cordova.InAppBrowser.open('https://www.facebook.com/LoveLaurenFashionBlog', '_blank', 'location=no');
});

$('body').on('click', '#twitter', function() {
  var ref = cordova.InAppBrowser.open('https://twitter.com/ArthursLauren', '_blank', 'location=no');
});

$('body').on('click', '#instagram', function() {
  var ref = cordova.InAppBrowser.open('https://www.instagram.com/lovelauren_eu/', '_blank', 'location=no');
});

$('.statusbar-overlay').css('background-color', '#000000');
