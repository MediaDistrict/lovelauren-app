module.exports = function(grunt) {

  // Config.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        src: 'www/js/app.js',
        dest: 'www/js/min/app.min.js'
      }
    },
    sass: {
        dist: {
            options: {
              style: 'compressed'
            },
            files: {
                'www/css/main.css': 'www/scss/main.scss',
            }
        }
    },
    watch: {
      options: {
        event: ['all']
      },
      css: {
        files: ['www/scss/*.scss', 'www/js/app.js'],
        tasks: ['sass', 'uglify']
      }
    },
    connect: {
      server: {
        options: {
          port: 9001,
          base: 'www',
          keepalive: true
        }
      }
    }
  });

  // Tasks.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Register tasks.
  grunt.registerTask('default', ['uglify', 'sass', 'watch']);

};
